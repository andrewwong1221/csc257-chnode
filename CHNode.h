#ifndef _CHNODE_H_
#define _CHNODE_H_

#define MAXNODES 8
#define FAILURE (BIDDING_INTERVAL/SENSE_INTERVAL)*2 
#define EXPIRE 8
#define ALL 255
#define MESSAGESIZE 16

typedef nx_struct {
    nx_uint8_t id;
    nx_uint8_t dest;
	nx_uint8_t last;
    nx_uint8_t isCH;
    nx_uint16_t data;  // This will probably change
	nx_uint8_t message[MESSAGESIZE];
} DataPkt;
#endif /* _CHNODE_H_ */

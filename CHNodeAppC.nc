#include <Timer.h>
#include "CHNode.h"
#define NEW_PRINTF_SEMANTICS
#define MAX_QUEUE_SIZE 10
configuration CHNodeAppC {}
implementation {
	components MainC;
	components LedsC;
	components PrintfC;
	components SerialStartC;
	components CHNodeC as App;
	components new TimerMilliC() as SenseTimer;
	components new TimerMilliC() as BiddingTimer;
	components new TimerMilliC() as CleaningTimer;
	components ActiveMessageC;
	components new AMSenderC(6);
	components new AMReceiverC(6);
	components new HamamatsuS10871TsrC() as PhotoSense;
	components new QueueC(message_t*, MAX_QUEUE_SIZE) as QueueC;
	components new PoolC(message_t, MAX_QUEUE_SIZE) as PoolC;
	components UserButtonC;
	

	App.Boot -> MainC;
	App.Leds ->LedsC;
	App.SenseTimer -> SenseTimer;
	App.BiddingTimer -> BiddingTimer;
	App.CleaningTimer -> CleaningTimer;
	App.Packet -> AMSenderC;
	App.AMPacket -> AMSenderC;
	App.AMControl -> ActiveMessageC;
	App.AMSend -> AMSenderC;
	App.Receive -> AMReceiverC;
	App.ReadLight -> PhotoSense.Read;
	App.MsgQueue -> QueueC.Queue;
	App.MsgPool -> PoolC.Pool;
	App.Get -> UserButtonC.Get;
	App.Notify -> UserButtonC.Notify;
}

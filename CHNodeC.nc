#include <Timer.h>
#include <UserButton.h>
#include "printf.h"
#include "CHNode.h"
#define BIDDING_INTERVAL 5000
#define SENSE_INTERVAL 250
#define DEBUG 0
module CHNodeC {
	uses {
		interface Boot;
		interface Leds;
		interface Timer<TMilli> as SenseTimer;
		interface Timer<TMilli> as BiddingTimer;
		interface Timer<TMilli> as CleaningTimer;
		interface Packet;
		interface AMPacket;
		interface AMSend;
		interface Receive;
		interface SplitControl as AMControl;
		interface Read<uint16_t> as ReadLight;
		interface Queue<message_t*> as MsgQueue;
		interface Pool<message_t> as MsgPool;
		interface Get<button_state_t>;
		interface Notify<button_state_t>;
	}
}

implementation {
    /* Method Declarations */
    void updateNodeData(uint8_t id, uint16_t data); 
    void setCHVal(bool val);
    void printData(DataPkt *pkt);
    uint16_t bid_result();
    void addToQueue(uint8_t id, uint8_t dest, uint8_t isCH, uint16_t data, char *msg);
    void printTable();
    task void sendPacket();
	void displayMsg(DataPkt *pkt);
	void sendMessage(uint8_t dest, char *msg);
	uint8_t findNextNode();
    /* End Method Declarations */

    /* Global Variables */
	uint8_t currentCH;
    uint16_t NoCHCount;
    uint16_t light;
    uint16_t NodeData[MAXNODES];
    uint16_t NodeAge[MAXNODES];
    bool busy = FALSE; // block simultaneous sending and receiving
    bool CH = FALSE;   // set to true if you are CH
	bool running = FALSE;
    /* End Global Variables */

	event void Notify.notify(button_state_t state) {
		if(state == BUTTON_PRESSED) {
			if(!running) {
				running = TRUE;
				printf("Starting node %d\n", TOS_NODE_ID);
				printfflush();
				call AMControl.start();
			}
			else {
				// If running send a message
				uint8_t next = findNextNode();
				char mymsg[16];
				printf("Sending message to %d\n", next);
				sprintf(mymsg, "Hello from %d", TOS_NODE_ID);
				sendMessage(next, mymsg);
				printfflush();
			}
		}
	}
    /* Called from Receive (set if you are the CH) */

    /* Start the ActiveMessage Controller */
    event void Boot.booted() {
		NoCHCount = 0;
		currentCH = 255;
		busy = FALSE;
		CH = FALSE;
		running = FALSE;
		call Leds.led0On();
		call Leds.led1On();
		call Leds.led2On();
		call Notify.enable();
    }

    /* Once finished starting the ActiveMessage Controllers start the timers */
    event void AMControl.startDone(error_t err) {
        if(err == SUCCESS) {
			call Leds.led0Off();
			call Leds.led1Off();
			call Leds.led2Off();
            call SenseTimer.startPeriodic(SENSE_INTERVAL);
            call CleaningTimer.startPeriodic(SENSE_INTERVAL);
        } else {
            call AMControl.start();
        }
    }
    
	/* Periodically delete old values from the sensor network to minimize the
	   chance of selecting a clusterhead that has been powered off */
    event void CleaningTimer.fired(){
        uint16_t i;
        for(i = 0; i < MAXNODES; i++){
            if(NodeAge[i] == EXPIRE){
                NodeAge[i] = 0;
                updateNodeData(i, 0);
            }else{
                NodeAge[i]+=1;
            }
        }
    }
    /* Periodically re-sample light and submit values */
    event void SenseTimer.fired() {
		if(currentCH != 255) {
			printf("CH Node: %d\n", currentCH);
		}
		else {
			printf("No CH Node\n");
		}
		printfflush();
        call ReadLight.read();
        updateNodeData(TOS_NODE_ID, light);
        // If not the node head then ensure CH exists and send out values
        if(!CH) {
            NoCHCount++;
            if(NoCHCount >= FAILURE) {
				uint8_t NewCH;
                printf("No CH node found\n");
                NewCH = bid_result();
				// If this nodes wins the bidding become CH
				// Reset NoCHCount
                if(NewCH == TOS_NODE_ID){
                    setCHVal(TRUE);
                    NoCHCount = 0;
                }
            }
        }
        // Send data
        addToQueue(TOS_NODE_ID, ALL, CH, light, 0);
        post sendPacket();
    } 

    void addToQueue(uint8_t id, uint8_t dest, uint8_t isCH, uint16_t data, char *msg) {
        message_t *tmp = call MsgPool.get();
        DataPkt *pkt;
		pkt = (DataPkt *)(call Packet.getPayload(tmp,(uint8_t)sizeof(DataPkt)));
        pkt->id = id;
		pkt->dest = dest;
		pkt->last = TOS_NODE_ID;
        pkt->isCH = (uint8_t) isCH;
        pkt->data = light;
		strncpy((char *)pkt->message, msg, 16);
        call MsgQueue.enqueue(tmp);
		post sendPacket();
    }

    task void sendPacket() {
        if(!busy) {
            message_t *tmp = call MsgQueue.head();
			DataPkt *pkt = (DataPkt *)tmp->data;
			am_addr_t pktdest = AM_BROADCAST_ADDR;
			if(pkt->dest != ALL) {
				// If the CH then send directly to node
				if(CH) {
					printf("Forwarding message to %d from %d\n", pkt->dest, pkt->id);
					printfflush();
					pktdest = (am_addr_t) pkt->dest;
				}
				// Otherwise forward to CH
				else if(!CH && currentCH != 255) {
					printf("Forwarding message to CH\n");
					printfflush();
					pktdest = (am_addr_t) currentCH;
				}
			}
            if(call AMSend.send(pktdest, tmp, sizeof(DataPkt)) == SUCCESS) {
#if DEBUG
				if(pkt->id != TOS_NODE_ID) {
					printf("Sending Packet\n");
					printData(pkt);
					printfflush();
				}
#endif
                call Leds.led0Toggle();
                busy = TRUE;
            }
        } else {
            if(!call MsgQueue.empty()) {
                //printf("Queue not empty calling sendPacket\n");
                post sendPacket();
            }
        }
    }
    event void ReadLight.readDone(error_t result, uint16_t data) {
        if(result == SUCCESS) {
            light = data;
        }
    }


    /* Once finished sending clear busy flag */
    event void AMSend.sendDone(message_t *sentmsg, error_t err) {
        message_t *tmp = call MsgQueue.dequeue();
        call MsgPool.put(tmp);
        busy = FALSE;
    }

    uint16_t bid_result() {
        int i;
        // Default winner is current node
        uint16_t winner = TOS_NODE_ID, maxVal;
        maxVal = 0;
        for(i = 0; i < MAXNODES; i++) {
            if(maxVal < NodeData[i]) {
                maxVal = NodeData[i];
                winner = i;
            }
        }
        if(winner != TOS_NODE_ID) {
            printf("New CH selected: %d\n", winner);
            printfflush();
        }
        return winner;
    }

    /* Pick the highest id and val pair and notify the entire network */
    event void BiddingTimer.fired() {
        uint16_t winner;
        // Sanity check (should always be CH)
        if(CH) {
			printTable();
            winner = bid_result();
            if(winner != TOS_NODE_ID) {
                // if the winner isn't you then broadcast who the new CH is 
                addToQueue(winner, ALL, (uint8_t)TRUE, (uint16_t) NULL, 0);
                post sendPacket();
                setCHVal(FALSE);
            }
        }
        //else do nothing
    }
    /* Stores the data from nodes */
    void updateNodeData(uint8_t id, uint16_t val) {
        // will store and pick the highest id
        uint16_t key;
        key = id % MAXNODES;
        NodeData[key] = val;
    }

    event void AMControl.stopDone(error_t err) {}
    event message_t* Receive.receive(message_t *receivemsg,
            void* payload,
            uint8_t len) {
        call Leds.led2Toggle();
        if(len == sizeof(DataPkt)) {
            DataPkt* pkt = (DataPkt*)payload;
            if(((bool)pkt->isCH) == TRUE) {
                //reset countdown timer
				currentCH = pkt->id;
                NoCHCount = 0;
            }
            // printData(pkt);
            //add the pkt information to bid process
            NodeAge[(pkt->id)%MAXNODES] = 0;
            updateNodeData(pkt->id, pkt->data);
            //need to check to see if we are now the new CH
            if(pkt->id == TOS_NODE_ID && pkt->isCH && pkt->dest == ALL) {
                setCHVal(TRUE);
            }
			// Dest is only set if message is sent
			if(pkt->dest == TOS_NODE_ID) {
				displayMsg(pkt);
			}
			// else forward packet
			else if(pkt->dest != ALL) {
				addToQueue(pkt->id, pkt->dest, 0, pkt->data, (char *)pkt->message);
			}
        }
        else {
            printf("Received bad packet, %d, %d\n", len, sizeof(DataPkt));
            printfflush();
        }
        return receivemsg;
    }

    void printData(DataPkt *pkt) {
        if(pkt->isCH == 1){
            printf("(ID %2u, Dest %2u,  is CH, Light value is %4u)\n", pkt->id, pkt->dest, pkt->data);
        }else{
            printf("(ID %2u, Dest %2u, not CH, Light value is %4u)\n", pkt->id, pkt->dest, pkt->data);
        }
        printfflush();
    }

    void printTable(){
        int i;
        printf("NodeData(%2u)\n", TOS_NODE_ID);
        for(i = 0; i < MAXNODES; i++) {
            printf("%4u ", i);
        }
        printf("\n");
        for(i = 0; i < MAXNODES; i++) {
            printf("%4u ", NodeData[i]);
        }
        printf("\n");
        printfflush();
    }
	
	void displayMsg(DataPkt *pkt) {
		printf("-------------------------\n");
		printf("Received msg from %d through %d\n", pkt->id, pkt->last);
		printf("%s\n", (char *)pkt->message);
		printf("-------------------------\n");
		printfflush();
	}

    void setCHVal(bool val) {
        CH = val;
		currentCH = TOS_NODE_ID;
        if(CH) {
            // Acctions to take if the CH
            // Enable BiddingTimer
            printf("Becoming CH\n");
            printfflush();
            call Leds.led1On();
            call BiddingTimer.startPeriodic(BIDDING_INTERVAL);

        } else {
            // Actions to take if not the CH
            // Stop the BiddingTimer
            call Leds.led1Off();
            call BiddingTimer.stop();
        }
    }
	void sendMessage(uint8_t dest, char *msg) {
		addToQueue(TOS_NODE_ID, dest, 0, light, msg);
	}
	uint8_t findNextNode() {
		int i;
		for(i = 1; i < MAXNODES; i++) {
			if(NodeData[(i+TOS_NODE_ID)%MAXNODES] != 0) {
				return (i+TOS_NODE_ID)%MAXNODES;
			}
		}
		return TOS_NODE_ID;
	}
}
